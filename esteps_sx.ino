#include <ArduinoBLE.h>
BLEService pressureService("1");
BLEDoubleCharacteristic pressure1("10", BLERead | BLENotify);
BLEDescriptor pressure1desc("100", "P1 Bar");

BLEDoubleCharacteristic pressure2("11", BLERead | BLENotify);
BLEDescriptor pressure2desc("110", "P1 Bar");

BLEDoubleCharacteristic pressure3("12", BLERead | BLENotify);
BLEDescriptor pressure3desc("120", "P1 Bar");

BLEDoubleCharacteristic pressure4("13", BLERead | BLENotify);
BLEDescriptor pressure4desc("130", "P1 Bar");

BLEDoubleCharacteristic pressure5("14", BLERead | BLENotify);
BLEDescriptor pressure5desc("140", "P1 Bar");

BLEDoubleCharacteristic pressure6("15", BLERead | BLENotify);
BLEDescriptor pressure6desc("150", "P1 Bar");


void setup()
{
   randomSeed(analogRead(0));
  pressure1.addDescriptor(pressure1desc);
  pressure2.addDescriptor(pressure2desc);
  pressure3.addDescriptor(pressure3desc);
  pressure4.addDescriptor(pressure4desc);
  pressure5.addDescriptor(pressure5desc);
  pressure6.addDescriptor(pressure6desc);
  //Serial.begin(9600);
  //while (!Serial)
    ;

  pinMode(LED_BUILTIN, OUTPUT);
  if (!BLE.begin())
  {
    //Serial.println("starting BLE failed!");
    while (1)
      ;
  }
  BLE.setLocalName("Left Insole");
  BLE.setAdvertisedService(pressureService);
  pressureService.addCharacteristic(pressure1);
  pressureService.addCharacteristic(pressure2);
  pressureService.addCharacteristic(pressure3);
  pressureService.addCharacteristic(pressure4);
  pressureService.addCharacteristic(pressure5);
  pressureService.addCharacteristic(pressure6);
  
  BLE.addService(pressureService);

  BLE.advertise();
  //Serial.println("Bluetooth device active, waiting for connections...");
}

void loop()
{
  BLEDevice central = BLE.central();

  if (central)
  {
    //Serial.print("Connected to central: ");
    //Serial.println(central.address());
    digitalWrite(LED_BUILTIN, HIGH);
    double pressures[]= {0,0,0,0,0,0};
    while (central.connected())
    {
      pressure1.writeValue(analogRead(A0) / 850.0);
      pressure2.writeValue( analogRead(A1) / 850.0);
      pressure3.writeValue( analogRead(A2) / 850.0);
      pressure4.writeValue( analogRead(A3) / 850.0);
      pressure5.writeValue(analogRead(A4) / 850.0);
      pressure6.writeValue( analogRead(A5) / 850.0);
      
      delay(800);
    }
  }
  digitalWrite(LED_BUILTIN, LOW);
  //Serial.print("Disconnected from central: ");
  //Serial.println(central.address());
}
